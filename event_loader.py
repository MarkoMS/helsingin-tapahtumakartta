"""
This file is part of "Helsingin tapahtumakartta".

"Helsingin tapahtumakartta" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Helsingin tapahtumakartta" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Helsingin tapahtumakartta".  If not, see <http://www.gnu.org/licenses/>.
"""
# Modified: Marko Suomalainen 2.3.2016

# !/usr/bin/python
# -*- coding: utf-8 -*-

import json
import urllib2
import dataset
import datetime
import event_filtering

"""
Daily run script that removes outdated event data and
downloads new event data from url.
Data is located in /static/data/data.db
"""
# TODO: run daily

database = dataset.connect('sqlite:///static/data/data.db')
table = database['data']


def get_events_before_datetime(dt):
    query_string = 'SELECT d.id FROM data d WHERE d.end_time < "' + str(dt) + '"'
    return event_filtering.do_query(query_string)


# removes outdated data from database
def remove_old_events():
    dt = datetime.datetime
    _result = get_events_before_datetime(dt.today())
    if list(_result)[0] != 'No results':
        for event in _result:
            print("Event " + event['id'] + " is old and will be deleted.")
            table.delete(id=event['id'])
            database['tag_links'].delete(event=event['id'])
    print("No old events to remove.")


# loads json data from url
def load_json_from_url(url):
    response = urllib2.urlopen(url)
    data = json.load(response, encoding='utf-8')
    return data


# loads new tags to database and links them to events
def load_tags(event):
    # for each tag check if such tag already exists
    # and link it to event
    tags = dict()
    tag_links = dict()
    _tags = database['tags']
    _tag_links = database['tag_links']
    for tag in event['keywords']:
        # check if tag exists, add tag if it doesnt
        n = tag['name']['fi']
        duplicate = _tags.find_one(name=n)
        if duplicate is None:
            tags['name'] = n
            _tags.insert(tags)

        # link tag to event
        tag_links['event'] = event['id'].encode('utf-8').translate(None, ':')
        tag_links['tag'] = n
        _tag_links.insert(tag_links)


# loads a page from helsinki events data
def load_page(page_number):
    events_loaded = 0

    # load json data from url
    url = "http://api.hel.fi/linkedevents/v0.1/event/?format=json&include=location%2Ckeywords&start=today&page="
    url += str(page_number)
    results = load_json_from_url(url)

    for event in results['data']:
        # TODO: could be improved using EXISTS query?
        # check if event already exists in database
        try:
            idd = event['id'].encode('utf-8').translate(None, ':')
        except KeyError:
            # event needs to have an id
            print("Event has no id, continue without loading.")
            continue
        duplicate = table.find_one(id=idd)
        if duplicate is not None:
            print("Duplicate of event " + idd + " found, continue without loading.")
            continue
        # check if event has all needed information:
        # start time, name and coordinates
        if event['start_time'] is None:
            print("Event " + idd + " has no start time, continue without loading.")
            continue
        if event['name'] is None:
            print("Event " + idd + " has no proper name, continue without loading.")
            continue
        else:
            # non-standard database doesn't have all needed attributes
            if 'fi' not in event['name']:
                print("Event " + idd + " has no proper finnish name, continue without loading.")
                continue
        if event['location'] is None:
            print("Event " + idd + " has no coordinates, continue without loading.")
            continue

        if event['description'] is None:
            print("Event " + idd + " has no description, continue without loading.")
            continue

        # load data if event is acceptable
        e = get_event(event)
        table.insert(e)
        load_tags(event)
        events_loaded += 1
        print("Event " + idd + " succesfully loaded.")

    print(str(events_loaded) + " events from page " + str(page_number) + " loaded.")

    # return true if next page exists
    return database['meta']['next'] != "", events_loaded


def parse_time(time):
    dt = datetime.datetime
    try:
        return dt.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
    except ValueError:
        try:
            return dt.strptime(time, "%Y-%m-%d")
        except ValueError:
            try:
                return dt.strptime(time, "%Y-%m-%d %H:%M:%S")
            except ValueError:
                return dt.strptime(time, "%Y-%m-%dT%H:%M:%S.%fZ")


def get_event(event):
    st = parse_time(event['start_time'])
    try:
        et = parse_time(event['end_time'])
    except TypeError:
        et = st.replace(hour=23, minute=59)

    tmp = dict()

    # id from data is added to id, because the native id in
    # sqlite only accepts integers and ids in data are strings
    tmp['id'] = event['id'].encode('utf-8').translate(None, ':')

    tmp['start_time'] = st
    tmp['end_time'] = et

    loc = event['location']
    tmp['longitude'] = loc['position']['coordinates'][0]
    tmp['latitude'] = loc['position']['coordinates'][1]

    tmp['name_fi'] = event['name'].get('fi')
    tmp['name_sv'] = event['name'].get('sv')
    tmp['name_en'] = event['name'].get('en')

    tmp['email'] = loc['email']

    tmp['description_fi'] = event['description'].get('fi')
    tmp['description_sv'] = event['description'].get('sv')
    tmp['description_en'] = event['description'].get('en')

    if loc['street_address'] is not None:
        tmp['address_fi'] = loc['street_address'].get('fi')
        tmp['address_sv'] = loc['street_address'].get('sv')
        tmp['address_en'] = loc['street_address'].get('en')

    if loc['address_locality'] is not None:
        tmp['city_fi'] = loc['address_locality'].get('fi')
        tmp['city_sv'] = loc['address_locality'].get('sv')
        tmp['city_en'] = loc['address_locality'].get('en')

    if event['info_url'] is not None:
        tmp['url'] = event['info_url']['fi']

    return tmp


def load_events(start_page, pages_to_load):
    t = datetime.datetime
    begin_time = t.today()
    remove_old_events()
    _events_loaded = 0
    pages_loaded = 0
    _start_page = start_page
    _pages_to_load = pages_to_load
    next_page = True
    while next_page and pages_loaded < _pages_to_load:
        result = load_page(_start_page)
        next_page = result[0]
        _events_loaded += result[1]
        _start_page += 1
        pages_loaded += 1
    begin_time = t.today() - begin_time
    print("Runtime: " + str(begin_time))
    if pages_loaded > 0 and _events_loaded > 0:
        print("Time per page: " + str(begin_time / pages_loaded))
        print("Time per event: " + str(begin_time / _events_loaded))

load_events(1, 20)
