/*
This file is part of "Helsingin tapahtumakartta".

"Helsingin tapahtumakartta" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Helsingin tapahtumakartta" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Helsingin tapahtumakartta".  If not, see <http://www.gnu.org/licenses/>.
 */
var map = L.map('map', {
    center: [60.2, 24.9],
    minZoom: 0,
    zoom: 11
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://openstreetmap.org" title="OpenStreetMap" target="_blank">OpenStreetMap </a>contributors'
    //subdomains: ['otile1', 'otile2', 'otile3', 'otile4']
}).addTo(map);

var myIcon = L.icon({
    iconUrl: '/static/img/pin24.png',
    iconRetinaUrl: '/static/img/pin48.png',
    iconSize: [29, 24],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14]
});

function selectEvent(e) {
    var hri_id = e.target.hri_id;
    $("#" + hri_id).trigger("click");
    setTimeout(function () {
        $('#list').scrollTop(0);
        $('#list').scrollTop($("#" + hri_id).offset().top - $('#list').offset().top + $('#list').scrollTop() - 100);
    }, 150);

}

$(document).ready(function(){
$.ajax({
        cache: false,
        url: "/static/data/query.json",
        dataType: "json",
        success: function (t) {
            markers = L.markerClusterGroup({
                maxClusterRadius: 20
            });
            markerlist = [];
            for (var i in t.results) {
                var marker = L.marker([t.results[i].latitude, t.results[i].longitude], {icon: myIcon})
                    .bindPopup(t.results[i].name_fi)
                    .on('click', selectEvent);
                marker.hri_id = t.results[i].id;
                markers.addLayer(marker);
                markerlist.push(marker);
            }
            map.addLayer(markers);
        }
    });

});


