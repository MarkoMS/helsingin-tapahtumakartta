/*
This file is part of "Helsingin tapahtumakartta".

"Helsingin tapahtumakartta" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Helsingin tapahtumakartta" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Helsingin tapahtumakartta".  If not, see <http://www.gnu.org/licenses/>.
*/

/* Kieliasetus: Ruotsi */

/* Modified: Marko Suomalainen 2.3.2016 */

function InitLangSv(date, dateformat) {
    setElementsText();
    setLocalDatePicker();
    setRadioButtonText();
    setDropdownText();
    setButtonText();


    function setElementsText() {
        $("#calendarStartLabel").text("Startdatum:");
        $("#calendarEndLabel").text("Slutdatum:");
        $("#pageTitle").text("Händelse karta över Helsingfors");
        $("#filterText").text("Begränsar sökning:");

        var crText = "Källan till uppgifterna: <a href='http://www.hri.fi/fi/dataset/linked-events-tapahtumarajapinta'>Linked events -händelse gränssnitt</a>. " +
            "Webmaster av detta material är Forum Virium Helsinki och ursprungliga författaren är Helsingin kaupungin kulttuurikeskus, Helsingin kaupunginkirjasto, " +
            "Helmet-kirjastopalvelu, Helsingin Markkinointi Oy. Material laddas från <a href='http://www.hri.fi/'>Helsinki Region Infoshare</a> -service på " +
            date + " och används under licens <a href='http://creativecommons.org/licenses/by/4.0/deed.fi'> Creative Commons 4.0 International (CC BY 4.0)</a>.";
        $("#copyrightText").html(crText);
    }

    function setLocalDatePicker() {
        $.datepicker.regional['sv'] = {
            prevText: '<föregäende', prevStatus: 'föregäende månad',
            nextText: 'nästa>', nextStatus: 'nästa mänad',
            monthNames: ['januari', 'februari', 'mars', 'april', 'maj', 'juni', 'juli',
                'augusti', 'september', 'oktober', 'november', 'december'],
            dayNamesMin: ['sön', 'mån', 'tis', 'ons', 'tors', 'fre', 'lör' ],
            dateFormat: dateformat, firstDay: 1,
            isRTL: false
        };

        $.datepicker.setDefaults($.datepicker.regional['sv']);
    }

    function setRadioButtonText(){
        $("#label_radio_buttons").text("Kombinera filtrering:");
        $("#label_radio_or").text("Eller");
        $("#label_radio_and").text("Och");
    }

    function setDropdownText() {
        $("#filter_dropdown").chosen('destroy');
        $("#filter_dropdown").attr("data-placeholder", "Välja filter...");
        $("#filter_dropdown").chosen({no_results_text: "Inga resultat hittades för:"});
    }

    function setButtonText() {
        $("#buttonSubmit").val("Överlämna");
    }
}