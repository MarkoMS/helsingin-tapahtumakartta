/*
This file is part of "Helsingin tapahtumakartta".

"Helsingin tapahtumakartta" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Helsingin tapahtumakartta" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Helsingin tapahtumakartta".  If not, see <http://www.gnu.org/licenses/>.
*/

/* Kieliasetus: Suomi */

/* Modified: Marko Suomalainen 1.3.2016 */

function InitLangFi(date, dateformat) {
    setElementsText();
    setLocalDatePicker();
    setRadioButtonText();
    setDropdownText();
    setButtonText();


    function setElementsText() {
        $("#calendarStartLabel").text("Aloituspäivä:");
        $("#calendarEndLabel").text("Lopetuspäivä:");
        $("#pageTitle").text("Helsingin tapahtumakartta");
        $("#filterText").text("Rajaa hakua:");

        var crText = "Datan lähde: <a href='http://www.hri.fi/fi/dataset/linked-events-tapahtumarajapinta'>Linked events -tapahtumarajapinta</a>. " +
            "Aineiston ylläpitäjä on Forum Virium Helsinki ja alkuperäinen tekijä Helsingin kaupungin kulttuurikeskus, Helsingin kaupunginkirjasto, " +
            "Helmet-kirjastopalvelu, Helsingin Markkinointi Oy. Aineisto on ladattu <a href='http://www.hri.fi/'>Helsinki Region Infoshare</a> -palvelusta " +
            date + " lisenssillä <a href='http://creativecommons.org/licenses/by/4.0/deed.fi'> Creative Commons Nimeä 4.0 Kansainvälinen (CC BY 4.0)</a>.";
        $("#copyrightText").html(crText);
    }

    function setLocalDatePicker() {
        $.datepicker.regional['fi'] = {
            prevText: '<edellinen', prevStatus: 'edellinen kuukausi',
            nextText: 'seuraava>', nextStatus: 'seuraava kuukausi',
            monthNames: ['tammikuu', 'helmikuu', 'maaliskuu', 'huhtikuu', 'toukokuu', 'kesäkuu',
                'heinäkuu', 'elokuu', 'syyskuu', 'lokakuu', 'marraskuu', 'joulukuu'],
            dayNamesMin: ['su', 'ma', 'ti', 'ke', 'to', 'pe', 'la'],
            dateFormat: dateformat, firstDay: 1,
            isRTL: false
        };

        $.datepicker.setDefaults($.datepicker.regional['fi']);
    }

    function setRadioButtonText(){
        $("#label_radio_buttons").text("Suodattimien yhdistäminen:");
        $("#label_radio_or").text("Tai");
        $("#label_radio_and").text("Ja");
    }

    function setDropdownText() {
        $("#filter_dropdown").chosen('destroy');
        $("#filter_dropdown").attr("data-placeholder", "Valitse suodattimet...");
        $("#filter_dropdown").chosen({no_results_text: "Osumia ei löytynyt haulle:"});
    }

    function setButtonText() {
        $("#buttonSubmit").val("Lähetä");
    }
}