/*
This file is part of "Helsingin tapahtumakartta".

"Helsingin tapahtumakartta" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Helsingin tapahtumakartta" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Helsingin tapahtumakartta".  If not, see <http://www.gnu.org/licenses/>.
*/

/* Kieliasetus: Englanti */

/* Modified: Marko Suomalainen 2.3.2016 */

function InitLangEng(date, dateformat) {
    setElementsText();
    setLocalDatePicker();
    setRadioButtonText();
    setDropdownText();
    setButtonText();


    function setElementsText() {
        $("#calendarStartLabel").text("Start date:");
        $("#calendarEndLabel").text("End date:");
        $("#pageTitle").text("Event map of Helsinki");
        $("#filterText").text("Limit search:");

        var crText = "Source of the data: <a href='http://www.hri.fi/fi/dataset/linked-events-tapahtumarajapinta'>Linked events -event interface</a>. " +
            "Webmaster of this material is Forum Virium Helsinki and original author is Helsingin kaupungin kulttuurikeskus, Helsingin kaupunginkirjasto, " +
            "Helmet-kirjastopalvelu, Helsingin Markkinointi Oy. Material is loaded from <a href='http://www.hri.fi/'>Helsinki Region Infoshare</a> -service on " +
            date + " and is used under a licence of <a href='http://creativecommons.org/licenses/by/4.0/deed.fi'> Creative Commons 4.0 International (CC BY 4.0)</a>.";
        $("#copyrightText").html(crText);
    }

    function setLocalDatePicker() {
        $.datepicker.regional['en'] = {
            prevText: '<previous', prevStatus: 'previous month',
            nextText: 'next>', nextStatus: 'next month',
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            dateFormat: dateformat, firstDay: 1,
            isRTL: false
        };

        $.datepicker.setDefaults($.datepicker.regional['en']);
    }

    function setRadioButtonText(){
        $("#label_radio_buttons").text("Combining filtering:");
        $("#label_radio_or").text("Or");
        $("#label_radio_and").text("And");
    }

    function setDropdownText() {
        $("#filter_dropdown").chosen('destroy');
        $("#filter_dropdown").attr("data-placeholder", "Choose filters...");
        $("#filter_dropdown").chosen({no_results_text: "No matches for entry:"});
    }

    function setButtonText() {
        $("#buttonSubmit").val("Submit");
    }
}



