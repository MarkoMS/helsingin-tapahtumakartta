"""
This file is part of "Helsingin tapahtumakartta".

"Helsingin tapahtumakartta" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Helsingin tapahtumakartta" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Helsingin tapahtumakartta".  If not, see <http://www.gnu.org/licenses/>.
"""

# Modified: Marko Suomalainen 3.3.2016

# !/usr/bin/python
# -*- coding: utf-8 -*-

import dataset
import json

# connect to database
database = dataset.connect('sqlite:///static/data/data.db')


# builds a query for loading events that happen between st and et
def get_events_by_datetime_str(st, et):
    if not st:
        st = "0000-01-01 00:00:00"
    if not et:
        et = "9999-01-01 00:00:00"
    query_string = 'SELECT d.id FROM data d WHERE '
    query_string += 'd.start_time BETWEEN datetime("' + st + '") AND datetime("' + et + '")'
    query_string += ' OR d.end_time BETWEEN datetime("' + st + '") AND datetime("' + et + '")'
    query_string += ' OR d.start_time < datetime("' + st + '") AND d.end_time > datetime("' + et + '")'
    return query_string


# builds a query for loading events with a tag
def get_events_with_tag_str(tag):
    if tag is None:
        return None
    query_string = 'SELECT d.id FROM data d, tag_links tl WHERE d.id = tl.event AND tl.tag = "' + tag + '"'
    return query_string


# builds a query string for getting events with tags
def get_events_with_tags_str(selection, *tags):
    query_string = 'SELECT d.id FROM data d WHERE'
    first = True
    for tag in tags:
        if tag is None:
            continue
        if not first:
            query_string += ' ' + selection
        else:
            first = False
        query_string += ' d.id IN (' + get_events_with_tag_str(tag) + ')'
    if query_string == 'SELECT d.id FROM data d WHERE':
        return None
    return query_string


# combines 2 queries and picks items that appear in both
def combine_queries(*query_strings):
    first = True
    query_string = 'SELECT d.* FROM data d WHERE'
    for query in query_strings:
        if query is None:
            continue
        if not first:
            query_string += ' AND'
        else:
            first = False
        query_string += ' d.id IN (' + query + ')'
    return query_string


# does a query specified in query_string
# either saves the query in query.json or returns the query
def do_query(query_string):
    result_length = 0
    results = database.query(query_string + " ORDER BY d.start_time")
    results = list(results)
    for result in results:
        result_length += 1
    if result_length == 0:
        results = {'No results'}

    dataset.freeze(results, format='json', filename='static/data/query.json')
    return results


def combine_same_events(json_url):
    with open(json_url, 'r') as f:
        json_data = json.load(f)

    results = json_data['results']

    if results[0] == 'No results':
        return

    delete_these = list()
    iterated_names = list()
    for e1 in results:
        print(e1)
        if e1['name_fi']:
            if e1['name_fi'] in iterated_names:
                continue
        iterated_names.append(e1['name_fi'])
        et = list()
        st = list()
        if type(e1['start_time']) is list:
            st.append(e1['start_time'][0])
            et.append(e1['end_time'][0])
        else:
            st.append(e1['start_time'])
            et.append(e1['end_time'])

        for e2 in results:
            if e2 is e1:
                continue
            if e1['name_fi'] == e2['name_fi']:
                st.append(e2['start_time'])
                et.append(e2['end_time'])
                delete_these.append(e2)

        e1['start_time'] = st
        e1['end_time'] = et

    for e in delete_these:
        results.remove(e)
        json_data['count'] -= 1

    with open(json_url, 'w') as f:
        f.write(json.dumps(json_data, indent=4))

"""
# test query
t = datetime.datetime
today = t.today()
tag_query = get_events_with_tags_str(None)
time_query = get_events_by_datetime_str(str(today), str(today + datetime.timedelta(days=30)))
time_query = get_events_by_datetime_str(None, None)
do_query(combine_queries(tag_query, time_query), True)
combine_same_events("static/data/query.json")
"""