"""
This file is part of "Helsingin tapahtumakartta".

"Helsingin tapahtumakartta" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Helsingin tapahtumakartta" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Helsingin tapahtumakartta".  If not, see <http://www.gnu.org/licenses/>.
"""

# Modified: Marko Suomalainen 3.3.2016

# !/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, redirect, url_for
import time
from datetime import date
import dataset
import re
import json
import event_filtering
import os

app = Flask(__name__)


def striphtml(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)


@app.route('/', methods=['GET', 'POST'])
def index():
    return redirect('/fi')


@app.route('/<lang>', methods=['GET', 'POST'])
def view(lang):
    #import ipdb; ipdb.set_trace()

    parameters = request.args
    selection = 'OR'

    start = 'Alkaa: '
    end = 'Loppuu: '
    if lang == 'sv':
        start = "Startar: "
        end = "Slutar: "
    elif lang == 'en':
        start = "From: "
        end = "To: "

    if parameters is not None:
        if parameters.get('calendarStart') is not None and parameters.get('calendarEnd') is not None:
            # Hae pvm ja hakusanoilla
            startdate = parameters.get('calendarStart')
            enddate = parameters.get('calendarEnd')
            filters = parameters.getlist('filter')
            selection = parameters.get('radio_selection')

            return render_template('index.html', date=time.strftime("%d.%m.%Y"), filters=convert_filters(filters),
                                   tags=get_tags(), events=get_events(startdate, enddate, filters, selection), language=lang,
                                   startdate=startdate, enddate=enddate, start_word=start, end_word=end, selection=selection)

        # oletushaku
        else:
            startdate = date.today().strftime("%d.%m.%Y")
            enddate = date.today().strftime("%d.%m.%Y")
            return render_template('index.html', date=time.strftime("%d.%m.%Y"), filters=convert_filters([]), tags=get_tags(),
                                   events=get_events(startdate, enddate, [], selection),
                                   language=lang, startdate=startdate, enddate=enddate, start_word=start, end_word=end, selection=selection)

    else:
        startdate = date.today().strftime("%d.%m.%Y")
        enddate = date.today().strftime("%d.%m.%Y")
        return render_template('index.html', date=time.strftime("%d.%m.%Y"), filters=convert_filters([]), tags=get_tags(),
                               events=get_events(startdate, enddate, [], selection),
                               language=lang, startdate=startdate, enddate=enddate, start_word=start, end_word=end, selection=selection)


def get_events(startdate, enddate, filters, selection):
    datesplit = str(startdate).split('.')
    startdate = datesplit[2] + '-' + datesplit[1] + '-' + datesplit[0] + ' 00:00:00'

    datesplit = str(enddate).split('.')
    enddate = datesplit[2] + '-' + datesplit[1] + '-' + datesplit[0] + ' 23:59:59'

    tag_query = event_filtering.get_events_with_tags_str(selection, *filters)
    time_query = event_filtering.get_events_by_datetime_str(str(startdate), str(enddate))
    num_of_events = event_filtering.do_query(event_filtering.combine_queries(tag_query, time_query))

    if num_of_events > 0 and list(num_of_events)[0] != 'No results':
        event_filtering.combine_same_events("static/data/query.json")
        root = os.path.realpath(os.path.dirname(__file__))
        json_url = os.path.join(root, "static/data", "query.json")
        data = json.load(open(json_url))

        datastring = striphtml(json.dumps(data))
        datajson = json.loads(datastring)
        events = datajson['results']

        for e in events:
            start = e['start_time'][0]
            y = start.split(' ')
            pvm = y[0]
            klo = (y[1])[0:5]
            p = pvm.split('-')
            join = p[2] + '.' + p[1] + '.' + p[0] + ' klo ' + klo
            e['start_time'] = join

            end = e['end_time'][0]
            y = end.split(' ')
            pvm = y[0]
            klo = (y[1])[0:5]
            p = pvm.split('-')
            join = p[2] + '.' + p[1] + '.' + p[0] + ' klo ' + klo
            e['end_time'] = join
    else:
        events = dict([])

    return events


def get_tags():
    db = dataset.connect('sqlite:///static/data/data.db')
    tags = []
    for tag in db['tags']:
        tags.append(tag['name'])
    return tags


def convert_filters(filters):
    return json.dumps(filters)


if __name__ == "__main__":
    portti = os.environ.get('PORT')
    if portti is None:
        app.run(debug=True)
    else:
        app.run(host='0.0.0.0', port=int(portti), debug=False)


